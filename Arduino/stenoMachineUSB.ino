#include <Keyboard.h>

bool bank0[8];
bool bank1[8];
bool bank2[8];
bool bank3[8];
bool bankCombined[32];

char  output[32];

int clkEn = 4;
int clkIn = 6;
int load = 7;

void setup() {
  for (int i = 0; i < 8; ++i){
    bank0[i] = 0;
    bank1[i] = 0;
    bank2[i] = 0;
    bank3[i] = 0;
  }
  for (int i = 0; i < 32; ++i) bankCombined[i] = 0;
  for (int i = 0; i < 32; ++i) output[i] = 0;

  pinMode(clkEn, OUTPUT);
  pinMode(load, OUTPUT);
  pinMode(clkIn, OUTPUT);
  pinMode(9, INPUT);
  pinMode(10, INPUT);
  pinMode(11, INPUT);

  Keyboard.begin();
  output[0 ] = 'q';
  output[1 ] = 'w';
  output[2 ] = 'e';
  output[3 ] = 'r';
  output[4 ] = 't';
  output[5 ] = 'u';
  output[6 ] = 'i';
  output[7 ] = 'o';
  output[8 ] = 'p';
  output[9] = '[';
  output[10] = 's';
  output[11] = 'd';
  output[12] = 'f';
  output[13] = 'j';
  output[14] = 'k';
  output[15] = 'l';
  output[16] = ';';
  output[17] = '\'';
  output[18] = 'c';
  output[19] = 'v';
  output[20] = 'n';
  output[21] = 'm';
  output[22] = '1';//Fake number bar input

  Serial.begin(115200);
  delay(100);
}

void loop() {

  //Pulse load pin
  digitalWrite(load, LOW);
  delayMicroseconds(5);
  digitalWrite(load, HIGH);
  delayMicroseconds(5);

  //START RX
  digitalWrite(clkEn, LOW);
  digitalWrite(clockIn, HIGH);

  uint8_t shiftRegister0 = 0;
  uint8_t shiftRegister1 = 0;
  uint8_t shiftRegister2 = 0;
  uint8_t shiftRegister3 = 0;

  //Read all shift registers:
  for (uint8_t i = 0; i < 8; ++i) {
    digitalWrite(clockIn, HIGH);
    shiftRegister0|= digitalRead(5) << i;
    shiftRegister1|= digitalRead(9) << i;
    shiftRegister2|= digitalRead(10) << i;
    //shiftRegister3|= digitalRead(11) << i;//Un-used 4th shift register
    digitalWrite(clockIn, LOW);
  }
  digitalWrite(clkEn, HIGH);
  //END RX

  //Sweep each input bank:
  for (int i = 7; i >= 0; --i ){
    bank0[7-i] = (shiftRegister0 >> i) & 0X01;
    bank1[7-i] = (shiftRegister1 >> i) & 0X01;
    bank2[7-i] = (shiftRegister2 >> i) & 0X01;
    bank3[7-i] = (shiftRegister3 >> i) & 0X01;
  }

  //Map individual banks of 8 inputs to a single 32 input array:
  for (int i = 0; i < 8; ++i) bankCombined[i] = bank0[i] ;
  for (int i = 0; i < 8; ++i) bankCombined[i+8] = bank1[i];
  for (int i = 0; i < 8; ++i) bankCombined[i+16] = bank2[i];
  for (int i = 0; i < 8; ++i) bankCombined[i+24] = bank3[i];

  //Sweep the inputs:
  for (int i = 0; i < 23; ++i) (bankCombined[i]) ? Keyboard.press(output[i]) : Keyboard.release(output[i]);
  delay(20);
}
